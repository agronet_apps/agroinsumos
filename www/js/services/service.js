//var URL="http://104.196.136.18/saviaWebApi/api/agroinsumo/";
var URL="http://serviciosmadr.minagricultura.gov.co/saviawebapi/api/agroinsumo/";
//var URL = "http://serviciosmadr.minagricultura.gov.co/saviawebapi/api/agroinsumo/http://186.155.199.197:8000/api/agroinsumo/";
angular.module('proyect.service', [] ) 
// se creo factori para la manupulacion de datos para que
// funcione se inyecta el nombre en los controladores

.factory('InfoService', ['$window','$http', function($window,$http){

  
//    var URL = "http://186.155.199.197:8000/api/agroinsumo/";
//    var URL = "http://192.168.1.134:8081/api/agroinsumo/";    

    var headers = {
        'Access-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    };

    var token = $window.localStorage['token'] || ""  ;
    var userLogin = $window.localStorage['userLOgin'] || " "  ;
    var insumoDetalle = {};
    var addingPrecioFormat = {};
    var categoryId;
    var favoriteAll = [];
    var IdMunicipio;
    var NameMunicipio;
    var NameDepartamento;
    //var municipio  =  $window.localStorage['municipio'] || "Ingrese su " ;
    var departamento = $window.localStorage['departamento'] || "Municipio " ;
    var municipio ;
    var LoadFister = $window.localStorage['LoadFister'] || " " ;
    var nombreUser = $window.localStorage['nameUser'] || " " ;
 

 
    var setMunicipio = function(idM, nameM , nameD, idD){
        console.log("setMunicipio idM: "+idM);
        console.log("setMunicipio idD: "+idD);



        $window.localStorage['municipio'] = nameM;
        $window.localStorage['departamento'] = nameD;
        $window.localStorage['municipioId'] = idM;
        $window.localStorage['departamentoId'] = idD;

        // IdMunicipio = idM;
        // NameMunicipio = nameM;
        // NameDepartamento = nameD;

        municipio = {
               idM : $window.localStorage.getItem('municipioId'),
               idD : $window.localStorage.getItem('departamentoId'),
               nameM : $window.localStorage.getItem('municipio'),
               nameD : $window.localStorage.getItem('departamento'),
          }       


        // //municipio = $window.localStorage.getItem('something');
        //  $localStorage['municipio'] = municipio;

    }

    var pasarNombre = function(){
        $http({
          method: 'POST',
          url: URL+'ObtenerPerfil',
          data: {"UserId":token},
          headers:headers,
        }).then(function(result) {
          $window.localStorage['nameUser'] = result.data.Data.Correo;

         }, function(error) { 

           console.log("ocurrio un error "+error);
        });
    }






    var setToken = function(tokenId){
        $window.localStorage['token'] = tokenId;
        // token = tokenId;
    }
    
   
    var setInfoInsumo = function(objet){
        insumoDetalle = objet;
        console.log("el detalle del unsumo es ------"+insumoDetalle);
    }

    var setAddPrecio = function(objet){
        addingPrecioFormat = objet;
    }
    var setCategoryId = function(cId){
        categoryId = cId;
    }

    var setUserLogin = function(boleam){
       $window.localStorage['userLOgin'] = boleam;
       // userLogin = $window.localStorage['userLOgin'] ;
        // userLogin = boleam;
    }

    var setLoadFister = function(boleam){
        $window.localStorage['LoadFister'] = boleam;
    }

    var insertFavoriteData = function(object){

        console.log("Insertando object")
        favoriteAll.push(object)
        createLocalStoregeFavorites()
        
    }

    var deteleObjectFavorite = function(object){

        //Eliminar del array General
        console.log("Eliminar services object")

        for (var i = 0; i < favoriteAll.length; i++){
            if (object.idObjec == favoriteAll[i].idObjec){
                favoriteAll.splice(i,1);
                
            }
        }
        createLocalStoregeFavorites()
    
    }
    var createSection = function(){
        
            console.log("Agregar");
            createISection(); 
            
        
    }

    var closeSection = function(){
        deleteDB();
    }

    //crear 
    function createISection(){
        $window.localStorage['token'] || " ";
        $window.localStorage['userLOgin'] || " ";  
    }

    // eliminar LocalStorege 
    function createLocalStoregeFavorites(){
        $window.localStorage['favortieData'] = angular.toJson(favoriteAll)   
    }

    function deleteDB(){
        console.log("Eliminar");
        $window.localStorage.removeItem('token');
        $window.localStorage.removeItem('userLOgin');
        $window.localStorage.removeItem('nameUser');
    }

    return {

        getToken: function(){
            return token;
        },

        setToken: function(token){
            setToken(token)
        },

        getUserLogin: function(){
            return userLogin;
        },

        setInfoInsumo: function(objet){
            setInfoInsumo(objet);
        },

        getDetalleInsumo: function(){
            return insumoDetalle;
        },

        setAddPrecio: function(objet){
            setAddPrecio(objet);
        },

        getAddPrecio : function(){
            return addingPrecioFormat;
        },

        setCategoryId: function(cId){
            setCategoryId(cId);
        },

        getCategoryId : function(){
            return categoryId;
        },

        insertFavorite: function(object){        
            insertFavoriteData(object)
        },
        deleteFavorite: function(object){
            deteleObjectFavorite(object)
        },

        getFavoriteAll: function(){
            return favoriteAll;
        },

        setUserLogin :function(boleam){
            setUserLogin(boleam);
        },

        getUserLogin : function(){
            return userLogin;
        },

        setMunicipio: function(idM, nameM, nameD, idD){
            setMunicipio(idM, nameM, nameD,idD);
        },

        getMunicipio : function(){


              municipio = {
                       idM : $window.localStorage.getItem('municipioId'),
                       idD : $window.localStorage.getItem('departamentoId'),
                       nameM : $window.localStorage.getItem('municipio'),
                       nameD : $window.localStorage.getItem('departamento')
                     }      

            return municipio;
        },

        getDepartamento : function(){
            return departamento;
        },

        closeSection : function(){
            closeSection();
        },

        createSection : function(){
            createSection();
        },

        setLoadFister : function(boleam){
            setLoadFister(boleam);
        },

        getloadFister: function(){
            return LoadFister;
        },
        pasarNombre: function(){
            pasarNombre();
        },
        getNameUser: function(){
            return nombreUser;
        }

 
    }



}])