angular.module('proyect.controllers.perfilCtrl',[])

.controller('perfilCtrl', 
  function($scope,InfoService, $http, SearchMunicipioInsumo,CambiarClave,$ionicLoading, $state,API) {

 
  $ionicLoading.show({
   template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
  });   

	$scope.API = API;

  var URL = $scope.API.url;

	var headers = {
		'Access-Control-Allow-Origin' : '*',
		'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
		'Content-Type': 'application/json',
		'Accept': 'application/json'
	};
 
	var userToken = InfoService.getToken();
  var IdMunicipio;
	$scope.perfil ={};

	$scope.userModel = {
	    nombreCompleto  : '',
      nombre2 : '',
      apellido : '',
      apellido2 : '',
      email : '',
      documentId: '',
      education: '',
      placeCity: '',
      direccion : '',
      genero: '',
      cellPhone: '',
      municipio2: '', 
      direccionProduccion: '',
      asociation: '',
      asociation2 : '',
      activity: '',
      category: '',
      productionPricipal: '',
      superfice: ''
    };

    var educativo ;

  // obtener desplegables 
  // http://192.168.1.134:8081/api/agroinsumo/ObtenerListasPerfil

  $http({
      method: 'POST',
      url: URL+'ObtenerListasPerfil',
      headers:headers,
   }).then(function(result) { 
          $scope.listaNivel = result.data.Data.NivelesEducativos; 
          $scope.obtenerPerfil();
       }, function(error) {
           console.log(error);
      });


   $scope.selectEducativo = function(nivel){
      educativo = nivel.Id;
      console.log(educativo);
      alert("hola");
   }



   $scope.obtenerPerfil =function(){

  // obtener datos 
  $http({
      method: 'POST',
      url: URL+'ObtenerPerfil',
      data: {"Token":userToken},
      headers:headers,
   }).then(function(result) {
          $ionicLoading.hide();  
          console.log(result);
          $scope.perfil = result.data.Data;

          $scope.userModel.municipio2  =  result.data.Data.MunicipioName;


          var educa = result.data.Data.NivelEducativo.toString();
          $scope.userModel.education = educa;
          // switch(result.data.Data.NivelEducativo) {

          //   case 1:
          //       $scope.userModel.education = "1";
          //       break;

          //   case 2:
          //      $scope.userModel.education = "2";
          //       break;

          //   case 3:
          //     $scope.userModel.education = "3";
          //     break;

          //   case 4:
          //     $scope.userModel.education = "4";
          //     break;

          //   case 0:
          //     $scope.userModel.education = "0";
          //     break;
                
          //   default:
          //       $scope.userModel.education = "0";
          // }



          switch(result.data.Data.IdCategoriaProducto) {

            case 1:
                $scope.userModel.category = "1";
                break;

            case 2:
               $scope.userModel.category = "2";
                break;

            case 3:
              $scope.userModel.category = "3";
              break;

            case 4:
              $scope.userModel.category = "4";
              break;

            case 0:
              $scope.userModel.category = "0";
              break;
                
            default:
                $scope.userModel.category = "0";
          }


          switch(result.data.Data.PerteneceAsociacion) {

            case true:
                $scope.userModel.asociation = "true";
                break;

            case false:
              $scope.userModel.asociation = "false";
              break;
                
            default:
                $scope.userModel.asociation = "false";
          }



          $scope.userModel.nombreCompleto = result.data.Data.Nombre;
          $scope.userModel.nombre2 = result.data.Data.SegundoNombre;
          $scope.userModel.apellido = result.data.Data.Apellido;
          $scope.userModel.apellido2 = result.data.Data.SegundoApellido;
          $scope.userModel.email = result.data.Data.Correo;
          $scope.userModel.documentId = result.data.Data.DocIdentidad;
          // $scope.userModel.placeCity = result.data.Data.IdMunicipio;
          $scope.userModel.direccion = result.data.Data.Direccion;
          $scope.userModel.genero = result.data.Data.Genero;
          $scope.userModel.cellPhone = result.data.Data.NumeroCelular;
          $scope.userModel.municipio2 =  result.data.Data.LugarDeProduccionName;

          $scope.userModel.asociation2 = result.data.Data.Asociacion;
          $scope.userModel.productionList = result.data.Data.LugarDeProduccion;
          $scope.userModel.activity = result.data.Data.IdActividadAgricola;
       }, function(error) {
          $ionicLoading.hide();  

           console.log(error);
           alert("Ha ocurrido un error");
      });
   }



  $scope.actualizarPerfil = function(){
    $ionicLoading.show({
     template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
    });   

    $http({
      method: 'POST',
      url: URL+'ActualizarPerfil',
      data: { "UserId":userToken,
              "Aplicacion":"insumos",
              "Nombre":$scope.userModel.nombreCompleto,
              "SegundoNombre":$scope.userModel.nombre2,
              "Apellido":$scope.userModel.apellido,
              "SegundoApellido":$scope.userModel.apellido2,
              "Correo":$scope.userModel.email,
              "DocIdentidad":$scope.userModel.documentId,
              "NivelEducativo":$scope.userModel.education,
              "IdMunicipio":IdMunicipio,
              "Direccion":$scope.userModel.direccion,
              "Genero":$scope.userModel.genero,
              "NumeroCelular":$scope.userModel.cellPhone,
              //"LugarDeProduccion":IdMunicipio,//$scope.userModel.municipios2,
              "PerteneceAsociacion":$scope.userModel.asociation,
              "Asociacion":$scope.userModel.asociation2,
              "NitAsociacion":"1587456",
              "IdActividadAgricola":$scope.userModel.activity,
              "IdCategoriaProducto":$scope.userModel.category 
            },
      headers:headers,
   }).then(function(result) {
         $ionicLoading.hide(); 
          console.log(result);
          
          alert("Sus datos han sido guardados");
          $state.go('home');
       }, function(error) {
           $ionicLoading.hide(); 
           console.log(error);
           alert("Ha ocurrido un error");
      });

  }

  $scope.userModel = {  "placeCity" : '' };

  $scope.searchMunicipio = function(){

      if(!$scope.userModel.placeCity == " ") {
        SearchMunicipioInsumo.searchMunicipios($scope.userModel.placeCity).then(
            function(matches) {
            $scope.userModel.municipios = matches;
          
            // $scope.imgCategoria = obtenerImgCategoria($scope.data.airlines.IdInsumo);
           }
        )
      }else{
         $scope.userModel.municipios = [];
      }
      // console.log($scope.userModel.municipios);
    }

    $scope.selectMunicipio = function(nameMunicipio, nameDepartamento, idM ){
        
        $scope.userModel.placeCity = nameMunicipio +", "+ nameDepartamento;

        $scope.userModel.municipios = [];
    }







    $scope.searchMunicipio2 = function(){
        
      if(!$scope.userModel.municipio2 == " ") {
        $scope.isMunicipio = true;
        console.log("Buscar2: "+$scope.userModel.municipio2);
        SearchMunicipioInsumo.searchMunicipios($scope.userModel.municipio2).then(
            function(matches) {

            $scope.userModel.municipios2 = matches;
          
            // $scope.imgCategoria = obtenerImgCategoria($scope.data.airlines.IdInsumo);
           }
        )
      }else{
        $scope.isMunicipio = false;
         $scope.userModel.municipios2 = [];
      }
      // console.log($scope.userModel.municipios);
    }

    $scope.selectMunicipio2 = function(nameMunicipio, nameDepartamento, idM ){
      $scope.isMunicipio = false;
      console.log("selectMunicipio2"+idM);
        IdMunicipio = idM;
        $scope.userModel.municipio2 = nameMunicipio +", "+ nameDepartamento;
        //$scope.userModel.municipio2 = [];
    }





  $scope.showPopup = function() {
    $state.go('recuperarContrasena');
  }


})

    


// busqueda de municipio
.factory('SearchMunicipioInsumo', function($q, $timeout, $http ) {

 
    var searchMunicipios = function(searchMini) { 

        $http({
        method: 'POST',
        url: URL+'ObtenerMunicipios',
        data: {"query":searchMini},
        headers:headers,
        }).then(function(result) {
            console.log(result.data);
              municipios = result.data.Data;
              var matches = municipios.filter(function(municipio) {
                if(municipio.Nombre.toLowerCase().indexOf(searchMini.toLowerCase()) !== -1 ) return true;
              })

           
            $timeout( function(){
            
               deferred.resolve( matches );

            }, 100);


           }, function(error) {
            console.log(error);
      });

        var deferred = $q.defer();

        return deferred.promise;

    };

    return { 

        searchMunicipios : searchMunicipios

    }
})




.service('CambiarClave', function($http, $log) {
    this.update = function(token,nueva,confirmacion) {
      var toSend={ "UserId":token,"Nueva":nueva,"Confirmacion":confirmacion}
       console.log("JSON TO SEND : "+angular.toJson(toSend))
       var promise  = $http({
           method : 'POST',
           url : URL+'CambiarContrasena',
           data: { "UserId":token,"Nueva":nueva,"Confirmacion":confirmacion},
           headers: headers,
       }).success(function(data, status, headers, config) {
           console.log("JSON CREADO : "+angular.toJson(data))
           customers = data;
           return customers;
       }).error(function(data,status, headers, config) {
       }); 
       return promise; 
    };
});