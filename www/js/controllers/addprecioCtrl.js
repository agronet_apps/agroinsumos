angular.module('proyect.controllers.addprecioCtrl',[])

.controller('addprecioCtrl', function($scope,InfoService, $http, $state, ProductoService,PuntoDeVentaService1,$ionicLoading, $ionicScrollDelegate, API, SearchMunicipioInsumo) {
	 
	     $scope.adding = true;
      $scope.API = API;
      // var URL = "http://192.168.1.134:8081/api/agroinsumo/";
      var URL = $scope.API.url;

      var headers = {
        'Access-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      };


    var userToken = InfoService.getToken();
    var dataAdding = InfoService.getAddPrecio();
    var categoria = InfoService.getCategoryId();
    var defIdCategoria;
    console.log("---------------"+dataAdding.idPresentacion);
    var productoId;
    var IdPv ;

    var idPresentAacion = dataAdding.idPresentacion;     

    $scope.precioModel = {
	   categoria  : '',
      producto : dataAdding.producto,
      presentacion: dataAdding.presentacion,
      ciudad: dataAdding.ciudad,
      direccion: '',
      nombrePuntoDeVenta: '',
      precio: '',
      comentario: '' 
    };
    

    if(!dataAdding.categoria == " "){
    	defIdCategoria = dataAdding.categoria;

    }
    if(!categoria == " "){
    	defIdCategoria = categoria;
    }

    console.log(defIdCategoria);
    console.log(categoria);
    console.log(dataAdding.categoria);

    productoId = dataAdding.idInsumo;

    switch(defIdCategoria) {

	    case 1:
	         $scope.precioModel.categoria = "1";
	        break;
	    case 2:
	       $scope.precioModel.categoria = "2";
	        break;
      case 3:
        $scope.precioModel.categoria = "3";
        break;
      case 4:
        $scope.precioModel.categoria = "4";
        break;
	    default:
	        $scope.precioModel.categoria = "0";
	}

	//Obtener imagen de la categoria
    $scope.imgCategoria = obtenerImgCategoria(defIdCategoria);
    //Obtener titulo de la categoria 
    $scope.nameCategoria = obtenerNameCategoria(defIdCategoria);



	var userToken = InfoService.getToken();
	
  	$scope.modificarDatos = function(){
  		$scope.adding = true;
      $ionicScrollDelegate.scrollTop();
      $state.go('addprecio');
  		// $route.reload();
  	}
  	
  	$scope.goConfirmar = function(){
      if (!$scope.precioModel.producto == " " && !$scope.precioModel.nombrePuntoDeVenta == " " && !$scope.precioModel.precio == " ") { 
 
    		$scope.adding = false;
        $ionicScrollDelegate.scrollTop();
        $state.go('addprecio');
    		// $route.reload();

      }else{
        alert("Los campos no pueden estar vacios");
      }
  	}
 
  	


 	//buscador
	// $scope.precioModel = {  "producto" : '' };

	//Peticion de busqueda
	$scope.searchProducto = function() {

  	if (!$scope.precioModel.producto == " ") {
      console.log("buscarProductor");
  	  ProductoService.searchAirlines($scope.precioModel.producto, $scope.precioModel.categoria ,userToken,IdMunicipio,IdDepartamento).then(
  	    function(matches) {
  	      console.log(matches);
  	      $scope.precioModel.airlines = matches;
  	    }
  	  )
  	}else{
  	  $scope.precioModel.airlines = [];
  	}

	}

	$scope.selectProducto = function(NombreInsumo, Cantidad, UnidadPresentacion ,IdInsumo, municipio , departamento, presentacion){
    
      $scope.precioModel.producto =  NombreInsumo ;
      $scope.precioModel.presentacion =  Cantidad + " "+ UnidadPresentacion;
      //$scope.precioModel.ciudad =  municipio + " "+departamento;
      productoId = IdInsumo; 
      idPresentAacion = presentacion;

      $scope.precioModel.airlines = [];
  }

  $scope.goToAddPV = function(){
  	$state.go('addPuntoVenta');
  }


  
  //Peticion de busqueda de puntos de venta 
  $scope.searchPuntoVenta = function() {
// ObtenerPuntoDeVenta
    if (!$scope.precioModel.nombrePuntoDeVenta == " ") {

      PuntoDeVentaService1.searchPV($scope.precioModel.nombrePuntoDeVenta, categoria, municipio_id).then(
        function(matches) {
          console.log(matches);
          $scope.precioModel.puntosVenta = matches;
        }
      )
    }else{
      $scope.precioModel.puntosVenta = [];
    }

  }

  $scope.selectPuntoVenta = function(nombrePV, idPv){
    
      $scope.precioModel.nombrePuntoDeVenta =  nombrePV ;
      IdPv = idPv;

      $scope.precioModel.puntosVenta = [];
  }

 

    $scope.searchMunicipio = function(){
      

      if(!$scope.precioModel.ciudad == " ") {
        console.log("Buscar: "+$scope.precioModel.ciudad);
        SearchMunicipioInsumo.searchMunicipios($scope.precioModel.ciudad).then(
            function(matches) {
            $scope.precioModel.municipios = matches;
          
            // $scope.imgCategoria = obtenerImgCategoria($scope.data.airlines.IdInsumo);
           }
        )
      }else{
         $scope.precioModel.municipios = [];
      }
      // console.log($scope.municipioDefaul.municipios);

    }

    $scope.selectMunicipio = function(nameMunicipio, nameDepartamento, idM, idD, idMun ){
        IdMunicipio = idM;
        IdDepartamento = idD;
        municipio_id = idMun;
        console.log(nameMunicipio);
        console.log(nameDepartamento);
        $scope.precioModel.ciudad =  nameMunicipio +", "+ nameDepartamento;
        $scope.precioModel.municipios = [];


    }




 
  $scope.addPrecio = function(){
      $ionicLoading.show({
       template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
      }); 
     $http({
      method: 'POST',
      url: URL+'AgregarPrecioReportado',
      data: { "IdPresentacionProducto":idPresentAacion,
          "Producto":productoId,
          "PuntoDeVenta":IdPv,
          "Marca":"1",
          "Precio":$scope.precioModel.precio,
          "Comentario":$scope.precioModel.comentario,
          "MunicipioId":IdMunicipio,
          "DepartamentoId":IdDepartamento,
          "Token":userToken
          },
      headers:headers,
    }).then(function(result) {
       $ionicLoading.hide();
        // $scope.favoritos = result.data.Data;
        alert("Precio Añadido correctamente ");
         $state.go('home');
         }, function(error) {
          $ionicLoading.hide();
          console.log(error);
    });


  }


})
 
//buscar productos
.factory('ProductoService', function( $q, $timeout, $http ) {

    // $scope.API = API;
    var searchAirlines = function(searchFilter , idCategoria ,token,idMunicipio,idDepartamento ) {
         
        // console.log('la busqueda es' + searchFilter);

        $http({
		    method: 'POST',
		    url: URL+'ObtenerInsumos',
		    data: {"CategoryId":idCategoria,"MunicipioId":idMunicipio, "DepartamentoId":idDepartamento, "Query":searchFilter,"Token":token},
		    headers:headers,
    		}).then(function(result) {
    		      airlines = result.data.Data;
    			    var matches = airlines.filter(function(airline) {
    			    	if(airline.NombreInsumo.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
    			    })

		       
		        $timeout( function(){
		        
		           deferred.resolve( matches );

		        }, 100);


		       }, function(error) {
		        console.log(error);
	    });

		    var deferred = $q.defer();

        return deferred.promise;

    };

    return {

        searchAirlines : searchAirlines

    }
})

// buscar punto de venta
.factory('PuntoDeVentaService1', function( $q, $timeout, $http ) {

    // $scope.API = API;
    var searchPV = function(searchFilter, categoria, municipio_id ) {
         
         console.log('la busqueda es' + searchFilter);
         console.log('la municipio_id es' + municipio_id);

        $http({
		    method: 'POST',
		    url: URL+'ObtenerPuntoDeVenta',
		    data: {"Id":"0","Municipio":municipio_id,"categoria":categoria,"IdPresentacion":"0","Query":searchFilter},
		    headers:headers,
    		}).then(function(result) {
    		      puntosVenta = result.data.Data;
    			    var matches = puntosVenta.filter(function(airline) {
    			    	if(airline.Nombre.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
    			    })

		       
		        $timeout( function(){
		        
		           deferred.resolve( matches );

		        }, 100);


		       }, function(error) {
		        console.log(error);
	    });

		    var deferred = $q.defer();

        return deferred.promise;

    };

    return {

        searchPV : searchPV

    }
})



// busqueda de municipo
.factory('SearchMunicipioInsumo', function($q, $timeout, $http ) {

 
    var searchMunicipios = function(searchMini) { 

        $http({
        method: 'POST',
        url: URL+'ObtenerMunicipios',
        data: {"query":searchMini},
        headers:headers,
        }).then(function(result) {
            console.log(result.data);
              municipios = result.data.Data;
              var matches = municipios.filter(function(municipio) {
                if(municipio.Nombre.toLowerCase().indexOf(searchMini.toLowerCase()) !== -1 ) return true;
              })

           
            $timeout( function(){
            
               deferred.resolve( matches );

            }, 100);


           }, function(error) {
            console.log(error);
      });

        var deferred = $q.defer();

        return deferred.promise;

    };

    return {

        searchMunicipios : searchMunicipios

    }
})


//retorna el Nombre de la categoria segun el id
var obtenerNameCategoria = function(idCategoria) {
  Categoria1 = "Fertilizantes";
  Categoria2 = "Medicamentos Veterinarios";
  Categoria3 = "Vacunas ";
  Categoria4 = "Plagicidas";
  categoria = "";

  switch(idCategoria) {

    case 1:
        categoria = Categoria1;
        break;

    case 2:
        categoria = Categoria4;
        break;

    case 3:
        categoria = Categoria2;
        break;

    case 4:
        categoria = Categoria3;
        break;  

    default:
        categoria = " ";
        break;
  }
   return categoria;
}


//retorna la imagen de la categoria segun el id
var obtenerImgCategoria = function(idCategoria) {
  Imagen1 = "Icon_fertilizante";
  Imagen2 = "Icon_medicamentos";
  Imagen3 = "icon_vacunas";
  Imagen4 = "icon_plaguicidas";
  Imagen = "";

  switch(idCategoria) {

    case 1:
        Imagen = Imagen1;
        break;

    case 2:
        Imagen = Imagen4;
        break;

    case 3:
        Imagen = Imagen2;
        break;

    case 4:
        Imagen = Imagen3;
        break;  

    default:
        Imagen = " ";
        break;
  }
  return Imagen;
};
