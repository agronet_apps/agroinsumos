angular.module('proyect.controllers.publicacionesCtrl',[])

.controller('publicacionesCtrl', function($scope,InfoService, $http, PuntoDeVentaService, $state,API,$ionicLoading, $ionicScrollDelegate,SearchMunicipioInsumo) {

  $ionicLoading.show({
   template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
  });  
 
 
	$scope.API = API;

  var URL = $scope.API.url;

  $scope.oneditar = true;

	var headers = {
		'Access-Control-Allow-Origin' : '*',
		'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
		'Content-Type': 'application/json',
		'Accept': 'application/json'
	};
 
  var productoId;
  var idPv ;
  var idpresentacion;
  var IdP;

  var categoria = InfoService.getCategoryId();

  var IdMunicipio;

	var userToken = InfoService.getToken();

      $http({
        method: 'POST',
        url: URL+'ObtenerPrecioReportado',
        data:{"Token":userToken},
        headers:headers,
     }).then(function(result) {
            $ionicLoading.hide();  
            console.log(result);
            $scope.publicaciones = result.data.Data;
            if(result.data.Data == " " ){
              $scope.publicaciones.texto = "No ha realizado publicaciones ";
            }

 
         }, function(error) {
             $ionicLoading.hide();  
             console.log(error);
             alert("Ha ocurrido un error"+error);
        });

 
  $scope.actionEdit = function(objet){
    $ionicScrollDelegate.scrollTop();

 
      $scope.publicacion = {
        producto : objet.ProductoName,
        presentacion: objet.ProductoPresentacionName,
        ciudad: objet.MunicipioName,
        direccion: '',
        nombrePuntoDeVenta: objet.PuntodeVentaText,
        precio: objet.Precio,
        comentario: objet.Comentario 
      };
      
      switch(objet.CategoriaId) {
   
        case 1:
             $scope.publicacion.categoria = "1";
            break;
        case 2:
           $scope.publicacion.categoria = "2";
            break;
        case 3:
          $scope.publicacion.categoria = "3";
          break;
        case 4:
          $scope.publicacion.categoria = "4";
          break;
        default:
            $scope.publicacion.categoria = "0";
      }




      productoId = objet.Id;
      idpresentacion = objet.IdPresentacionProducto;
      IdP = objet.Producto;
      idPv = objet.PuntoDeVenta;


      $scope.oneditar = false;

  }


  $scope.actionDelete = function(objet){
      $ionicLoading.show({
         template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
      });  


       $http({
        method: 'POST',
        url: URL+'EliminaPrecioReportado',
        data: { "Token":userToken,"Id":objet.Id
          },
        headers:headers,
     }).then(function(result) {
            $ionicLoading.hide();
            console.log(result);
            alert(result.data.Texto);
            $scope.oneditar = false;
            $state.go('home');

         }, function(error) {
             $ionicLoading.hide();
             console.log(error);
             alert("Ha ocurrido un error"+error);
        });


  }

    
  $scope.goConfirmar = function(){
    
  $scope.oneditar = true;
    $http({
        method: 'POST',
        url: URL+'EditarAgregarPrecioReportado',
        data: { "Id": productoId ,
          "IdPresentacionProducto":idpresentacion,
          "Producto":IdP,
          "PuntoDeVenta":idPv,
          "Marca":"1",
          "Precio":$scope.publicacion.precio,
          "Comentario":$scope.publicacion.comentario,
          "Token":userToken
          },
        headers:headers,
     }).then(function(result) {
            console.log(result);
            alert(result.data.Texto);
            $scope.oneditar = true;
            $state.go('home');

         }, function(error) {
             console.log(error);
             alert("Ha ocurrido un error"+error);
        });


  }


  //Peticion de busqueda de puntos de venta 
  $scope.searchPuntoVenta = function() {
    $ionicLoading.show({
         template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
      }); 
  // ObtenerPuntoDeVenta
    if (!$scope.publicacion.nombrePuntoDeVenta == " ") {
      PuntoDeVentaService.searchPV($scope.publicacion.nombrePuntoDeVenta, categoria).then(
        function(matches) {
          $ionicLoading.hide();
          console.log(matches);
          $scope.publicacion.puntosVenta = matches;
        }
      )
    }else{
      $scope.publicacion.puntosVenta = [];
    }

  }

  $scope.selectPuntoVenta = function(nombrePV, IdPv){
    
      $scope.publicacion.nombrePuntoDeVenta =  nombrePV ;
      idPv = IdPv;

      $scope.publicacion.puntosVenta = [];
  }


  //busca municipios 
  $scope.searchMunicipio = function(){
      
      console.log("buscarMunicipio:-"+$scope.publicacion.ciudad+"-");
      if(!$scope.publicacion.ciudad == " ") {
        console.log("Buscar: "+$scope.publicacion.ciudad);
        SearchMunicipioInsumo.searchMunicipios($scope.publicacion.ciudad).then(
            function(matches) {
            $scope.publicacion.municipios = matches;
          
            // $scope.imgCategoria = obtenerImgCategoria($scope.data.airlines.IdInsumo);
           }
        )
      }else{
         $scope.publicacion.municipios = [];
      }
      // console.log($scope.municipioDefaul.municipios);

    }

    $scope.selectMunicipio = function(nameMunicipio, nameDepartamento, idM ){
      console.log("selectMunicipio");
        IdMunicipio = idM;
        console.log(nameMunicipio);
        console.log(nameDepartamento);
        console.log(IdMunicipio);
        $scope.publicacion.ciudad =  nameMunicipio +", "+ nameDepartamento;
        $scope.publicacion.municipios = [];


    }





})



// buscar punto de venta
.factory('PuntoDeVentaService', function( $q, $timeout, $http ) {

    // $scope.API = API;
    var searchPV = function(searchFilter) {
         
        // console.log('la busqueda es' + searchFilter);

        $http({
        method: 'POST',
        url: URL+'ObtenerPuntoDeVenta',
        data: {"Id":"0","Municipio":"723","IdPresentacion":"0","Query":searchFilter},
        headers:headers,
        }).then(function(result) {
              puntosVenta = result.data.Data;
              var matches = puntosVenta.filter(function(airline) {
                if(airline.Nombre.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
              })

           
            $timeout( function(){
            
               deferred.resolve( matches );

            }, 100);


           }, function(error) {
            console.log(error);
      });

        var deferred = $q.defer();

        return deferred.promise;

    };

    return {

        searchPV : searchPV

    }
})


// busqueda de municipo
.factory('SearchMunicipioInsumo', function($q, $timeout, $http ) {

 
    var searchMunicipios = function(searchMini) { 

        $http({
        method: 'POST',
        url: URL+'ObtenerMunicipios',
        data: {"query":searchMini},
        headers:headers,
        }).then(function(result) {
              console.log(result.data);
              municipios = result.data.Data;
              var matches = municipios.filter(function(municipio) {
                if(municipio.Nombre.toLowerCase().indexOf(searchMini.toLowerCase()) !== -1 ) return true;
              })

           
            $timeout( function(){
            
               deferred.resolve( matches );

            }, 100);


           }, function(error) {
            console.log(error);
      });

        var deferred = $q.defer();

        return deferred.promise;

    };

    return {

        searchMunicipios : searchMunicipios

    }
})