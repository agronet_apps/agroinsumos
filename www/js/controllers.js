var airlines = [{}];
var municipios = [{}];
    

var headers = {
	'Access-Control-Allow-Origin' : '*',
	'Access-Control-Allow-Methods' : 'POST, GET, OPTIONS, PUT',
	'Content-Type': 'application/json',
	'Accept': 'application/json'
};

// Id categoria seleccionada por el usuario
 
var idCategoria = 0 ;
var IdInsumos = 0;
var MunicipioId;
var desde= 0;
// var userLog = true;


    //var URL = API.url;

//  var URL = "http://186.155.199.197:8000/api/agroinsumo/";f
//  var URL = "http://192.168.1.134:8081/api/agroinsumo/";
// Registro


angular.module('proyect.controllers', [])

 
// Login controller
.controller('loginCtrl', function($scope, $http ,$state,InfoService,$ionicLoading,$ionicModal) {

  // http://192.168.1.134:8081/api/agro/IniciarSesion

 
 
  // datos ingresados por el usuario
  $scope.login = {
  	email : '',
  	password: ''
  };   

  var token = InfoService.getToken();

  if(!token == ""){
    $state.go('home');
  }
 

  //form login/rregistro
  $scope.existUser = true;
  $scope.textButton = "Crear una cuenta"
  $scope.typeInput = "password"

  var stateViewPassword = true;

  $scope.existUserAction = function(){
    $scope.existUser = false;
    $scope.textButton =  "Ingresar"
  }

  $scope.viewPassword = function(){
    if (stateViewPassword) {
      //Ver
      stateViewPassword = false
      $scope.typeInput = "text"
      
    }else {
      stateViewPassword = true
      $scope.typeInput = "password"
    }
  }

  $scope.onSubmit = function(valid){
    
    console.log(valid);

    //validamos que los datos vengan correctos
    if (valid) {
      $ionicLoading.show({
       template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
      });
      //validamos en  que vista estamos 
      if ($scope.existUser) {
        //si estanmos en registro, registramos los datos 
        $http({
            method: 'POST',
            url: URL+'Registro',
            data: { 
                  "Correo":$scope.login.email,
                  "Clave" :$scope.login.password,
                  "AppId" :"0",
                  "TokenPush": "q239890afa8904"
                  },
            headers:headers,
         }).then(function(result) {
                 $ionicLoading.hide(); 
                  console.log(result);
                  $scope.existUser = false;
                  if (result.data.Estado == 1) {
                    alert(result.data.Texto);
                    $state.go('registro-result');
                  }else if(result.data.Estado == 0){
                    alert(result.data.Texto +" ingrese por favor");
                    console.log(result);
                    $state.go('login');
                  }
               }, function(error) {
                   $ionicLoading.hide(); 
                   console.log(error);
                   alert("Ha ocurrido un error");
              });

      }else if (!$scope.existUser){
        //si estanmos en registro, ingresamos el perfil
        $http({
            method: 'POST',
            url: URL+'IniciarSesion',
            data: { 
                  "Email":$scope.login.email,
                  "password" :$scope.login.password,
                  "AppId" :"0",
                  "TokenPush": "q239890afa8904"
                  },
            headers:headers,
         }).then(function(result) { 
                  $ionicLoading.hide(); 
                  console.log(result);
                  if (result.data.Estado == 1) {
                    InfoService.setLoadFister(true);
                    InfoService.setUserLogin(true);
                    InfoService.createSection();
                    InfoService.setToken(result.data.Token);
                    $state.go('registro-result');
                    window.location.reload();
                  }else if(result.data.Estado == 0){
                    alert(result.data.Texto);
                    console.log(result);
                  }
               }, function(error) {
                   $ionicLoading.hide(); 
                   console.log(error);
                   alert("ocurrio error");
                  return error;
              });
      }      
    } else {
      alert("Verifique sus datos");
    }

  }

})

//controlador de vista olvidoContrasena
.controller('olvidoContrasenaCtrl', function($scope, $http, API,$state,$ionicLoading) {

  $scope.errorEmail = false;
  $scope.enviado = false;

  $scope.textButton = "Enviar";

    $scope.formModel = {
      email : ''
    };


  // http://localhost:50145/api/AgroTeConecta/Olvido

  $scope.recuperarContrasena = function(){
    $ionicLoading.show({
     template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
    });
    
    if (!$scope.formModel.email == " ") {
    $http({
        method: 'POST',
        url: URL+'RecoveryPass',
        data: { "Correo":$scope.formModel.email},
        headers:headers,
      }).then(function(result) {
            $ionicLoading.hide(); 
             if (result.data.Estado == 0) {
                $scope.errorEmail = true;
                 alert(result.data.Texto);
             }else if (result.data.Estado == 1) {
                alert(result.data.Texto);
                $scope.enviado = true;
             }
         }, function(error) {
             alert("Ocurrio un error en el servidor");
             $ionicLoading.hide(); 
             console.log(error);
             console.log(" respuesta NEGATIVA");
      });
      
    }else{
      alert("el campo no puede estar vacio");
    };


  }
  
})













//controlador categorias
.controller('categoriaCtrl', function($scope, $state,InsumoService ,InfoService,$http,SearchMunicipioInsumo,$ionicLoading) {

  $ionicLoading.show({
     template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
  }); 

  var token = InfoService.getToken();


  if (token == "") {
    console.log(token);
    $scope.isLog = false;
  }else{
    console.log(token);
    $scope.isLog = true;
  }

 

  var municipio = InfoService.getMunicipio();
     MunicipioId = municipio.idM;
     DepartamentoId = municipio.idD;
     nameMunicipio = municipio.nameM;
     nameDepartamento =municipio.nameD;



  //var departamento = InfoService.getDepartamento();

  $scope.municipioDefaul = {
    busqueda: ''
  } 

  $scope.municipioDefaul = {  "busqueda" : '' };

  $scope.searchLoadingMunicipioFertilizante =false;
  $scope.searchMunicipio = function(){
    console.log("buscar municipio");
    if(!$scope.municipioDefaul.busqueda == " ") {
      $scope.searchLoadingMunicipioFertilizante =true;
      SearchMunicipioInsumo.searchMunicipios($scope.municipioDefaul.busqueda).then(
          function(matches) {
            $scope.searchLoadingMunicipioFertilizante =false;
          $scope.municipioDefaul.municipios = matches;
        
          // $scope.imgCategoria = obtenerImgCategoria($scope.data.airlines.IdInsumo);
         }
      )
    }else{
      $scope.searchLoadingMunicipioFertilizante =false;
       $scope.municipioDefaul.municipios = [];
    }
    // console.log($scope.municipioDefaul.municipios);
  }

  $scope.selectMunicipio = function(nameMunicipio, nameDepartamento, idM, idD){
     console.log("Selecciono el municipio: "+nameMunicipio+" IdMunicipio: "+idM+ " DepartamentoId: "+idD);

      MunicipioId = idM;
      DepartamentoId = idD;
      $scope.municipioDefaul.busqueda = nameMunicipio +", "+ nameDepartamento;

      $scope.municipioDefaul.municipios = [];
      InfoService.setMunicipio(idM,nameMunicipio,nameDepartamento, idD);
      $scope.searchInMunicipio();
  }




  $scope.municipioDefaul.busqueda = nameMunicipio + " " + nameDepartamento;




  var categoria = InfoService.getCategoryId();
  $scope.insumos ={};
  var token = InfoService.getToken();
  //poblar lista  
     console.log("buscar<<<<"+ MunicipioId);
     console.log("buscar<<<<"+ DepartamentoId);

    $scope.searchInMunicipio = function() { InsumoService.searchAirlines("", categoria, MunicipioId, DepartamentoId).then(
        function(matches) {
          $ionicLoading.hide();
          console.log("acaa");
          angular.forEach(matches, function(value, key){
            if (value.Favorito == false) {
              value["starFav"] = "icon-star_line_off";
            }else if (value.Favorito == true) {
              value["starFav"] = "icon-star_line_on";
            };
              console.log(value.Favorito);
          });

          $scope.insumos = matches;
          console.log("acaaxx");
        }
      )
    }
   $scope.searchInMunicipio();

    //Funcion Autocompletable dentro de categoria
    $scope.data = {  "searchInCategoria" : '' };
    $scope.searchLoadingFertilizante=false;

    $scope.searchInCategoria = function() {
      $scope.searchLoadingFertilizante=true;
      console.log("buscar en categoria");
      if (!$scope.data.searchInCategoria == " ") {
        
        InsumoService.searchAirlines($scope.data.searchInCategoria, categoria,MunicipioId, DepartamentoId).then(
          function(matches) {
            $scope.searchLoadingFertilizante=false;
            $scope.data.airlines = matches;
          }
        )
      }else{
      $scope.data.airlines = [];
      $scope.searchLoadingFertilizante=false;
      }  
    }



    // //obtener id de insumo
    //   $scope.pasId = function(idInsumo){
    //   IdInsumos = idInsumo ;
    // }

    //Obtener imagen de la categoria
   $scope.imgCategoria = obtenerImgCategoria(categoria);
   //Obtener titulo de la categoria 
   $scope.nameCategoria = obtenerNameCategoria(categoria);
  
   $scope.goToInsumo = function(idIns, idGeo, idPret){
      var dataInsumo = {
        Id : idIns,
        Municipio : idGeo,
        idPret : idPret
      }

      InfoService.setInfoInsumo(dataInsumo);
      console.log("id insumo:"+dataInsumo.Id+"id municipio:"+dataInsumo.Municipio+"id presentacion:"+dataInsumo.idPret);

      $state.go('insumo');

   }
    $scope.iconFavorite = "icon-star_line_on"
    $scope.textStateFavorite = "Quitar de favoritos";
    
   $scope.actionFavorite = function(insumoObjet){      
    //loanding
    $ionicLoading.show({
     template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
    }); 
    if (insumoObjet.Favorito){


        $http({
          method: 'POST',
          url: URL+'Favorito',
          data: {
            "Token":token,
            "Id":insumoObjet.IdInsumo,
            "Estado":"false",
            "IdPresentacion":insumoObjet.IdPresentacionProducto,
            "IdGeografia":insumoObjet.IdGeografia
          },
          headers:headers,
        }).then(function(result) {
              insumoObjet.starFav = "icon-star_line_off";
              $ionicLoading.hide();
              alert(result.data.Texto);
           }, function(error) {
            $ionicLoading.hide();
             alert("error");
            console.log(error);
        });

        // InfoService.deleteFavorite(object) 
    }else if (!insumoObjet.Favorito){
        
      $http({
        method: 'POST',
        url: URL+'Favorito',
        data: {
          "Token":token,
          "Id":insumoObjet.IdInsumo,
          "Estado":"True",
          "IdPresentacion":insumoObjet.IdPresentacionProducto,
          "IdGeografia":insumoObjet.IdGeografia
        },
        headers:headers,
      }).then(function(result) {
             insumoObjet.starFav = "icon-star_line_on";
            $ionicLoading.hide();
            alert(result.data.Texto);

            $route.reload();
         }, function(error) {
          $ionicLoading.hide();
          alert("error");
          console.log(error);
      });   
    }
      
  }

})



















//controlador Detalle insumo
.controller('insumoCtrl', function($scope , $http ,InfoService, SearchMunicipioInsumo, $state, $cordovaSocialSharing,$ionicLoading,$cordovaDialogs) {

  $ionicLoading.show({
     template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
  }); 
  var dataInsumo = InfoService.getDetalleInsumo();
 
  var IdMunicipio ;
  var token = InfoService.getToken();

  $scope.read = false;
  $scope.read2 = false;
  $scope.stateButtonYes = 'circleLabelYes'
  $scope.stateButtonNo = 'circleLabelNo'



  if (token == "") {
    console.log(token);
    $scope.isLog = false;
  }else{
    console.log(token);
    $scope.isLog = true;
  }

  $scope.municipioDefaul = {
    busqueda : ''
  }

  $scope.detalleInsumo = {};
  $scope.groups = [];

  //servicio obtener detalle de insumo 
  $http({
      method: 'POST',
      url: URL+'ObtenerDetalleInsumo',
      data: {"Id":dataInsumo.Id,
        "Municipio":dataInsumo.Municipio,
        "Presentacion":dataInsumo.idPret,
        "Token" : token
      },
      headers:headers,
  }).then(function(result) {
            $ionicLoading.hide();
            var matches = result.data.Data;
            if (result.data.Estado==1)
            {
                      if (!token == "") {
                            if (matches.Favorito == false) {
                              // matches["starFav"] = "icon-star_line_off";
                              $scope.starFav = "star2";
                              $scope.titleFavorito = "Añadir a Favoritos";
                            }else if (matches.Favorito == true) {
                              // $scope.starFav = "icon-star_line_on";
                              $scope.starFav = "star1";
                              $scope.titleFavorito = "Quitar de Favoritos";
                            };
                          }    
                    
                      console.log(matches.starFav);
                      console.log(matches.titleFavorito);
               

                   $scope.detalleInsumo = matches;
                   console.log(JSON.stringify($scope.detalleInsumo.PuntosDeVentas.Nombre));
                   $scope.detalleInsumo.Marcas.NombreMarca;
                   $scope.detalleInsumo.PuntosDeVentas.Nombre;
                   $scope.detalleInsumo.Historicos.Precio;

                  console.log(JSON.stringify($scope.detalleInsumo.PuntosDeVentas.Nombre));
                   console.log(JSON.stringify($scope.detalleInsumo.PuntosDeVentas.Precios));


                  $scope.municipioDefaul = {
                    busqueda :$scope.detalleInsumo.Municipio +" , "+ $scope.detalleInsumo.Departamento
                  }


                  //Obtener imagen de la categoria
                  $scope.imgCategoria = obtenerImgCategoria($scope.detalleInsumo.CategoriaId);
                  //Obtener titulo de la categoria 
                  $scope.nameCategoria = obtenerNameCategoria($scope.detalleInsumo.CategoriaId);

            }
            else if (result.data.Estado==0){

            }
           else{
             alert("Ocurrio un error en el servidor");
           }
    
            
         }, function(error) {
          $ionicLoading.hide();
          console.log(error);
      });
    

    // //vistas desplegables

      $scope.desplegado1 = false;
      $scope.desplegado2 = false;
      $scope.desplegado3 = false;
    $scope.desplegar = function(num){
      if (num==1) {
          $scope.desplegado1 = true;
          $scope.desplegado2 = false;
          $scope.desplegado3 = false;
      }else if (num==2) {
          $scope.desplegado1 = false;
          $scope.desplegado3 = false;
          $scope.desplegado2 = true;
      }else if (num==3) {
          $scope.desplegado3 = true;
          $scope.desplegado1 = false;
          $scope.desplegado2 = false;
      };
    }



    // addprecio
    $scope.goToAddPrecio = function(){
      var addingPrecioFormat = {
        categoria: $scope.detalleInsumo.CategoriaId,
        producto: $scope.detalleInsumo.NombreInsumo,
        presentacion: $scope.detalleInsumo.Cantidad + " " + $scope.detalleInsumo.UnidadPresentacion,
        ciudad: $scope.detalleInsumo.Municipio +" , "+ $scope.detalleInsumo.Departamento,
        namepv:'',
        precio:'',
        comentarios:'',
        idInsumo: $scope.detalleInsumo.IdInsumo,
        idMunicipio: $scope.detalleInsumo.CodigoMunicipio,
        idPresentacion: $scope.detalleInsumo.IdPresentacionProducto

      }
      InfoService.setAddPrecio(addingPrecioFormat);
      console.log(addingPrecioFormat);
      $state.go('addprecio');

    }
     
    //le fue util la informacion 
    $scope.yea = function(state){
      
      if (!token == ""){

          if($scope.read){                
              return
          }

          if (state) {

              $http({
                  method: 'POST',
                  url: URL+'MarcarComoUtil',
                  data:{"Id":dataInsumo.Id,"Estado":true},
                  headers:headers,
               }).then(function(result) {

                   }, function(error) {
                       alert("Ha ocurrido un error");
                  });
              $scope.stateButtonYes = 'circleLabelYes2'
              $scope.stateButtonNo = 'circleLabelNo'
              $scope.read = true 
          
          }else{
          
             $http({
                method: 'POST',
                url: URL+'MarcarComoUtil',
                data:{"Id":dataInsumo.Id,"Estado":false},
                headers:headers,
             }).then(function(result) {

                 }, function(error) {
                     alert("Ha ocurrido un error"+error);
                });
              $scope.stateButtonYes = 'circleLabelYes'
              $scope.stateButtonNo = 'circleLabelNo2'
              $scope.read = true
          }

          $cordovaDialogs.alert(
              'Tu opion es muy importante para nosotros', 
              'Gracias', 
              'Ok')

          .then(function() {
              console.log("button Ok")          
          });
                  


      }else{

           $cordovaDialogs.alert(
              'Usuario no registrado, por favor registrate', 
              'Error', 
              'Ok')

          .then(function() {
            console.log("button Ok")
          });

      }

    }

    $scope.municipioDefaul = {  "busqueda" : '' };

    $scope.searchMunicipio = function(){
      

      if(!$scope.municipioDefaul.busqueda == " ") {
        console.log("Buscar: "+$scope.municipioDefaul.busqueda);
        SearchMunicipioInsumo.searchMunicipios($scope.municipioDefaul.busqueda).then(

            function(matches) {
            $scope.municipioDefaul.municipios = matches;
          
            // $scope.imgCategoria = obtenerImgCategoria($scope.data.airlines.IdInsumo);
           }
        )
      }else{
         $scope.municipioDefaul.municipios = [];
      }
      // console.log($scope.municipioDefaul.municipios);

    }
 
    $scope.selectMunicipio = function(nameMunicipio, nameDepartamento, idM ){
        IdMunicipio = idM;
        console.log(nameMunicipio);
        console.log(nameDepartamento);
        $scope.municipioDefaul = {
          busqueda : nameMunicipio +", "+ nameDepartamento
        }
        $scope.municipioDefaul.municipios = [];


    }
  
    $scope.share =  function(name, precio, cantidad, unidad){
      console.log("/////////// share");
      console.log("name "+name);
      console.log("precio "+precio);
      console.log("cantidad "+cantidad);
      console.log("unidad "+unidad);
      var value = parseInt(precio);
      $cordovaSocialSharing.share(
          "El precio promedio a nivel nacional del insumo " + name + " /" + cantidad + " "+ unidad + " es de  $" + value ,
          "Costo promedio de " + name + " " +  cantidad +" "+ unidad,
          null,
          "https://play.google.com/store"
      )
       
    }  

    $scope.actionFavorite = function(insumoObjet){     


    if($scope.read2){                
      return
    }
    //loanding
    $ionicLoading.show({
     template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Por favor espere un momento....'
    }); 

    if (insumoObjet.Favorito){

        $http({
          method: 'POST',
          url: URL+'Favorito',
          data: {
            "Token":token,
            "Id":insumoObjet.IdInsumo,
            "Estado":"false",
            "IdPresentacion":insumoObjet.IdPresentacionProducto,
            "IdGeografia":insumoObjet.IdGeografia
          },
          headers:headers,
        }).then(function(result) {
              $scope.starFav = "star2";
              $scope.titleFavorito = "Añadir a Favoritos"; 
              $scope.read2 = true;
              $ionicLoading.hide();
              alert(result.data.Texto);
           }, function(error) {
            $ionicLoading.hide();
            alert("ha ocurrido un error ");
            console.log(error);
        });

        // InfoService.deleteFavorite(object) 
    }else if (!insumoObjet.Favorito){
        
      $http({
        method: 'POST',
        url: URL+'Favorito',
        data: {
          "Token":token,
          "Id":insumoObjet.IdInsumo,
          "Estado":"True",
          "IdPresentacion":insumoObjet.IdPresentacionProducto,
          "IdGeografia":insumoObjet.IdGeografia
        },
        headers:headers,
      }).then(function(result) {
            $scope.starFav = "star1";
            $scope.titleFavorito = "Quitar de Favoritos";
            $scope.read2 = true;
            $ionicLoading.hide();
            alert(result.data.Texto);
         }, function(error) {
          $ionicLoading.hide();
          alert("ha ocurrido un error");
          console.log(error);
      });   
    }
      
  }

})   















// controlador home
.controller('homeCtrl', ['$scope', 'InsumoService','InfoService', 'SearchMunicipioInsumo','$state', '$http',  function($scope , InsumoService,InfoService,SearchMunicipioInsumo,$state,$http, API) {          
  var token = InfoService.getToken();       
  var municipio = InfoService.getMunicipio();
  var departamento = InfoService.getDepartamento();
  console.log("municipio: "+JSON.stringify(municipio));
  console.log(municipio.idM);
  console.log(municipio.nameM + " " + municipio.nameD);

  MunicipioId = municipio.idM;
  DepartamentoId= municipio.idD;

  if (MunicipioId===null||DepartamentoId===null){
    DepartamentoId=11;
    MunicipioId=1;
    municipio="Bogotá D.C.";
    departamento="Bogotá D.C.";
    InfoService.setMunicipio(MunicipioId,municipio,departamento, DepartamentoId);
    var municipio = InfoService.getMunicipio();
    var departamento = InfoService.getDepartamento();

  }
 //obtener nombre del personaje 

  

  if (token == "") {
    console.log(token);
    $scope.userLog = false;
  }else{
    console.log(token);
    $scope.userLog = true;

    $http({
      method: 'POST',
      url: URL+'ObtenerPerfil',
      data: {"Token":token},
      headers:headers,
    }).then(function(result) {
      console.log("/ObtenerPerfil: "+JSON.stringify(result.data));
      $scope.nombrePersonaje = result.data.Data.Nombre+ " "+result.data.Data.Apellido;

     }, function(error) { 

    });

  }
  //$scope.nombrePersonaje = InfoService.getNameUser();

  $scope.municipioDefaul = {
    busqueda: ''
  } 
 



  var usLo = InfoService.getUserLogin(); 
  var loadFister = InfoService.getloadFister();




  $scope.municipioDefaul = {  "busqueda" : '' };

  $scope.municipioDefaul.busqueda = municipio.nameM + " " + municipio.nameD;

  $scope.searchLoadingMunicipio=false;

  $scope.searchMunicipio = function(){

    if(!$scope.municipioDefaul.busqueda == " ") {
      $scope.searchLoadingMunicipio=true;
      console.log("Buscar: "+$scope.municipioDefaul.busqueda);
      SearchMunicipioInsumo.searchMunicipios($scope.municipioDefaul.busqueda).then(
          function(matches) {
            $scope.searchLoadingMunicipio=false;
          $scope.municipioDefaul.municipios = matches;
        
          // $scope.imgCategoria = obtenerImgCategoria($scope.data.airlines.IdInsumo);
         }
      )
    }else{
       $scope.searchLoadingMunicipio=false;
       $scope.municipioDefaul.municipios = [];
    }
    // console.log($scope.municipioDefaul.municipios);
  }
 
  $scope.selectMunicipio = function(nameMunicipio, nameDepartamento, idM, idD ){
      console.log("nameMunicipio: "+nameMunicipio);
      console.log("idM: "+idM);
      console.log("->>>>idD: "+idD);
      MunicipioId = idM;
      DepartamentoId =idD;
      $scope.municipioDefaul.busqueda = nameMunicipio +", "+ nameDepartamento;

      $scope.municipioDefaul.municipios = [];
      InfoService.setMunicipio(idM,nameMunicipio,nameDepartamento, idD);
  }





//TODO aca cambiar a 0 para que busque en todo
  InfoService.setCategoryId(1);

  //Obtener imagen de la categoria
  var categoria = InfoService.getCategoryId();
  
  $scope.categoriaId = function(categoriaId){
      idCategoria = categoriaId;
      InfoService.setCategoryId(categoriaId);

  }


  //buscador
  $scope.data = {  "search" : '' };
  $scope.searchLoading=false;
  //Peticion de busqueda
  $scope.search = function() {
      console.log("buscar");
      $scope.searchLoading=true;

    if (!$scope.data.search == " ") {

      InsumoService.searchAirlines($scope.data.search, categoria,MunicipioId,DepartamentoId).then(
        function(matches) {
          $scope.searchLoading=false;
          angular.forEach(matches, function(value, key){
              value["NameImagen"] = obtenerImgCategoria(value.CategoriaId);
              
          });
          $scope.data.airlines = matches;
          // $scope.imgCategoria = obtenerImgCategoria($scope.data.airlines.IdInsumo);
        }
      )
    }else{
      $scope.searchLoading=false;
      $scope.data.airlines = [];
    }

  }

  //obtener id de insumo
  $scope.pasId = function(idInsumo){
    IdInsumos = idInsumo ;
  }

  $scope.closeSesion = function(){
    InfoService.setUserLogin(false);
    InfoService.setToken(" ");
    InfoService.closeSection();
    // $scope.$route.reload();
    
    // $state.go('splash');
    $state.reload();
    window.location.reload(true);
    
    // $route.reload();
    $scope.userLog = false;

    // Stop the ion-refresher from spinning
     $scope.$broadcast('scroll.refreshComplete');
   
  }

  $scope.goToInsumo = function(idIns, idGeo, idPret){
    var dataInsumo = {
      Id : idIns,
      Municipio : idGeo,
      idPret : idPret
    }

    InfoService.setInfoInsumo(dataInsumo);
    console.log("id insumo:"+dataInsumo.Id+"id municipio:"+dataInsumo.Municipio+"id presentacion:"+dataInsumo.idPret);

    $state.go('insumo');

  }


}])


// ------------------- factorias 

// servicio Autocompletable Busqueda de insumos 


.factory('InsumoService', function( $q, $timeout, $http,InfoService ) {

    // $scope.API = API;
    var searchAirlines = function(searchFilter , idCategoria, municipioId, departamentoId) {
        var token = InfoService.getToken();
         console.log('la busqueda es: ' + searchFilter);
         console.log('la busqueda es idCategoria: ' + idCategoria);
         console.log('municipioId: ' + municipioId); 
         console.log('departamentoId: ' + departamentoId); 
        $http({
		    method: 'POST',
		    url: URL+'ObtenerInsumos',
		    data: {"CategoryId":idCategoria,"MunicipioId":municipioId,"DepartamentoId":departamentoId, "Query":searchFilter,"Token":token},
		    headers:headers,
    		}).then(function(result) {
              if(result.data.Estado!=-1){
                airlines = result.data.Data;
                            var matches = airlines.filter(function(airline) {
                              if(airline.NombreInsumo.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1 ) return true;
                            })

                    $timeout( function(){
                    
                       deferred.resolve( matches );

                    }, 100);
              }
        		  else{
                deferred.resolve();
                alert("ocurrio un error en el servidor");
              }


		       }, function(error) {
		        console.log(JSON.stringify(error));
	    });

		    var deferred = $q.defer();

        return deferred.promise;

    };

    return {

        searchAirlines : searchAirlines

    }
})


// busqueda de municipo
.factory('SearchMunicipioInsumo', function($q, $timeout, $http ) {

 
    var searchMunicipios = function(searchMini) { 

        $http({
        method: 'POST',
        url: URL+'ObtenerMunicipios',
        data: {"query":searchMini},
        headers:headers,
        }).then(function(result) {
              console.log(result.data);
              municipios = result.data.Data;
              var matches = municipios.filter(function(municipio) {
                if(municipio.Nombre.toLowerCase().indexOf(searchMini.toLowerCase()) !== -1 ) return true;
              })

           
            $timeout( function(){
            
               deferred.resolve( matches );

            }, 100);


           }, function(error) {
            console.log(error);
      });

        var deferred = $q.defer();

        return deferred.promise;

    };

    return {

        searchMunicipios : searchMunicipios

    }
})



//retorna el Nombre de la categoria segun el id
var obtenerNameCategoria = function(idCategoria) {
  Categoria1 = "Fertilizantes";
  Categoria2 = "Medicamentos Veterinarios";
  Categoria3 = "Vacunas ";
  Categoria4 = "Plagicidas";
  categoria = "";

  switch(idCategoria) {

    case 1:
        categoria = Categoria1;
        break;

    case 2:
        categoria = Categoria4;
        break;

    case 3:
        categoria = Categoria2;
        break;

    case 4:
        categoria = Categoria3;
        break;  

    default:
        categoria = " ";
        break;
  }
   return categoria;
}


//retorna la imagen de la categoria segun el id
var obtenerImgCategoria = function(idCategoria) {
  Imagen1 = "Icon_fertilizante";
  Imagen2 = "Icon_medicamentos";
  Imagen3 = "icon_vacunas";
  Imagen4 = "icon_plaguicidas";
  Imagen = "";

  switch(idCategoria) {

    case 1:
        Imagen = Imagen1;
        break;

    case 2:
        Imagen = Imagen4;
        break;

    case 3:
        Imagen = Imagen2;
        break;

    case 4:
        Imagen = Imagen3;
        break;  

    default:
        Imagen = " ";
        break;
  }
  return Imagen;
};

